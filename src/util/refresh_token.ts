import {securityId, UserProfile} from '@loopback/security';
import {promisify} from 'util';
const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);

export async function generateRefreshToken(
  userProfile: UserProfile,
): Promise<string> {
  return await signAsync(
    {
      id: userProfile[securityId],
      name: userProfile.name,
      email: userProfile.email,
    },
    process.env.SECRET_REFRESH_TOKEN_KEY,
    {
      expiresIn: '1d',
    },
  );
}
