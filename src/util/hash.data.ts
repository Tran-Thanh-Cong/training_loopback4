import * as bcrypt from 'bcrypt';

export async function hashData(data: string): Promise<string | undefined> {
  return await bcrypt.hash(data, await bcrypt.genSalt(10));
}
