import * as bcrypt from 'bcrypt';

export async function compareData(
  data: string,
  hashData: string,
): Promise<boolean> {
  return await bcrypt.compare(data, hashData);
}
