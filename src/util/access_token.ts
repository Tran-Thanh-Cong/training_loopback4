import {HttpErrors} from '@loopback/rest';
import {securityId, UserProfile} from '@loopback/security';
import {promisify} from 'util';
import {responseError} from '../validator/validate.response';
const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);

export async function generateAccessToken(
  userProfile: UserProfile,
): Promise<string> {
  return await signAsync(
    {
      id: userProfile[securityId],
      name: userProfile.name,
      email: userProfile.email,
    },
    process.env.SECRET_ACCESS_TOKEN_KEY,
    {
      expiresIn: '60s',
    },
  );
}

export async function verifyAccessToken(token: string): Promise<UserProfile> {
  try {
    if (!token) throw new HttpErrors.Unauthorized('Token not found');
    const decodedToken = await verifyAsync(
      token,
      process.env.SECRET_ACCESS_TOKEN_KEY,
    );
    console.log(decodedToken);
    return {
      [securityId]: decodedToken.id,
      name: decodedToken.name,
      email: decodedToken.email,
    };
  } catch (error) {
    return responseError(error.message);
  }
}
