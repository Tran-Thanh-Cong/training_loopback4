import {AuthenticationStrategy} from '@loopback/authentication';
import {inject} from '@loopback/core';
import {HttpErrors, Request, RestBindings} from '@loopback/rest';
import {UserProfile} from '@loopback/security';
import {verifyAccessToken} from '../util/access_token';

export class JWTAuthenticationStrategy implements AuthenticationStrategy {
  name: string = 'jwt';
  constructor(@inject(RestBindings.Http.REQUEST) public request: Request) {}

  async authenticate(): Promise<UserProfile> {
    const token: string = this.getTokenFromBrowser(this.request);
    const userProfile: UserProfile = await verifyAccessToken(token);
    return userProfile;
  }

  getTokenFromBrowser(request: Request): string {
    if (!request.headers.authorization) {
      throw new HttpErrors.Unauthorized('Authorization header not found');
    }
    const authorizationToken = request.headers.authorization;
    if (!authorizationToken.startsWith('Bearer')) {
      throw new HttpErrors.Unauthorized('Authorization header not correct');
    }
    const token = authorizationToken.split(' ')[1];
    return token;
  }
}
