// Uncomment these imports to begin using these cool features!

import {repository} from '@loopback/repository';
import {post, requestBody} from '@loopback/rest';
import {UserProfile} from '@loopback/security';
import * as isEmail from 'isEmail';
import {UsersRepository} from '../repositories';
import {response} from '../types/response.type';
import {UsersLogin, UsersRegister} from '../types/user.type';
import {generateAccessToken} from '../util/access_token';
import {compareData} from '../util/compare.data';
import {generateRefreshToken} from '../util/refresh_token';
import {responseError, responseSuccess} from '../validator/validate.response';

// import {inject} from '@loopback/core';

export class AuthController {
  constructor(
    @repository(UsersRepository) public usersRepository: UsersRepository,
  ) {}

  @post('/register')
  async register(@requestBody() user: UsersRegister) {
    try {
      if (user.password.length < 8) {
        return responseError('Password must be at least 8 characters');
      }
      if (!isEmail.validate(user.email)) {
        return responseError('Email not a valid email');
      }
      if (await this.usersRepository.findUserByEmail(user.email)) {
        return responseError('Email already exists');
      }
      const data = await this.usersRepository.createUser(user);
      return responseSuccess('Create user successfully', data);
    } catch (error) {
      return responseError(error.message);
    }
  }

  @post('login')
  async login(@requestBody() user: UsersLogin): Promise<response | undefined> {
    try {
      const userModel = await this.usersRepository.findUserByEmail(user.email);
      if (!userModel) {
        return responseError('User not found');
      }
      if (!(await compareData(user.password, userModel.password))) {
        return responseError('Password not match');
      }
      const userProfile: UserProfile =
        this.usersRepository.convertToUserProfile(userModel);
      const [access_token, refresh_token] = await Promise.all([
        generateAccessToken(userProfile),
        generateRefreshToken(userProfile),
      ]);
      await this.usersRepository.updateUser(userModel, refresh_token);
      return responseSuccess('Login successful', {
        access_token: access_token,
        refresh_token: refresh_token,
      });
    } catch (error) {
      return responseError(error.message);
    }
  }

  // // em truyền tạm refresh_token qua body
  // @post('/refresh-token')
  // async refreshToken(@requestBody() token: any, request: Request) {
  //   try {
  //     if (!this.checkExpToken(request)) {
  //       const userModel = await this.usersRepository.findUserByRefreshToken(
  //         token.refresh_token,
  //       );
  //       if (userModel) {
  //         const userProfile =
  //           this.usersRepository.convertToUserProfile(userModel);
  //         const access_token = await generateAccessToken(userProfile);
  //         return responseSuccess('Refresh token successfully', {
  //           access_token: access_token,
  //           refresh_token: token.refresh_token,
  //         });
  //       } else {
  //         return responseError('Refresh token failed');
  //       }
  //     } else {
  //       return responseError('Token may van dung dc');
  //     }
  //   } catch (error) {
  //     return responseError(error.message);
  //   }
  // }

  // //check access_token expiresIn
  // async checkExpToken(request: Request): Promise<boolean> {
  //   const tokenHeader = request.headers.authorization;
  //   if (tokenHeader === undefined) {
  //     return false;
  //   }
  //   const {exp} = await verifyAccessToken(tokenHeader.split(' ')[1]);
  //   if (Date.now() >= exp * 1000) {
  //     return false;
  //   }
  //   return true;
  // }
}
