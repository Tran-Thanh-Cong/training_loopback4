// Uncomment these imports to begin using these cool features!

import {authenticate} from '@loopback/authentication';
import {repository} from '@loopback/repository';
import {get} from '@loopback/rest';
import {Users} from '../models';
import {UsersRepository} from '../repositories';
import {responseError, responseSuccess} from '../validator/validate.response';

// import {inject} from '@loopback/core';

export class UsersController {
  constructor(
    @repository(UsersRepository) public usersRepository: UsersRepository,
  ) {}

  @authenticate('jwt')
  @get('/all')
  async getAllUsers(): Promise<Users[]> {
    try {
      const data = await this.usersRepository.getAllUsers();
      return responseSuccess('Success', data);
    } catch (error) {
      return responseError(error.message);
    }
  }
}
