import {response} from '../types/response.type';

enum StatusCode {
  SUCCESS_CODE = 200,
  ERROR_CODE = 400,
}

export function response(
  message: string,
  code: number,
  data: any,
): response | any {
  return {
    message: message,
    code: code,
    data: data,
  };
}

export function responseSuccess(message: string, data: any): response | any {
  return {
    message: message,
    code: StatusCode.SUCCESS_CODE,
    data: data,
  };
}
export function responseError(message: string, data = {}): response | any {
  return {
    message: message,
    code: StatusCode.ERROR_CODE,
    data: data,
  };
}
