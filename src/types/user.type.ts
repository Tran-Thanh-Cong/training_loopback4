export interface UsersLogin {
  email: string;
  password: string;
}

export interface UsersRegister extends UsersLogin {
  username: string;
}
