import {inject} from '@loopback/core';
import {Count, DefaultCrudRepository} from '@loopback/repository';
import {securityId, UserProfile} from '@loopback/security';
import {DbDataSource} from '../datasources';
import {Users, UsersRelations} from '../models';
import {UsersRegister} from '../types/user.type';
import {hashData} from '../util/hash.data';

export class UsersRepository extends DefaultCrudRepository<
  Users,
  typeof Users.prototype.id,
  UsersRelations
> {
  constructor(@inject('datasources.db') dataSource: DbDataSource) {
    super(Users, dataSource);
  }

  async createUser(user: UsersRegister): Promise<Users | undefined> {
    return await this.create({
      username: user.username,
      email: user.email,
      password: await hashData(user.password),
    });
  }

  async getAllUsers(): Promise<Users[]> {
    return await this.find({});
  }

  async findUserByEmail(
    email: string,
  ): Promise<(Users & UsersRelations) | null> {
    return await this.findOne({where: {email: email}});
  }

  async updateUser(user: Users, refresh_token: string): Promise<Count> {
    return await this.updateAll(
      {
        username: user.username,
        email: user.email,
        password: user.password,
        refresh_token: refresh_token,
      },
      {id: user.id},
    );
  }

  async findUserById(id: number): Promise<Users> {
    return await this.findById(id);
  }

  async findUserByRefreshToken(
    refreshToken: string,
  ): Promise<(Users & UsersRelations) | null> {
    return await this.findOne({where: {refresh_token: refreshToken}});
  }

  convertToUserProfile(user: Users): UserProfile {
    return {
      [securityId]: `${user.id}`,
      name: user.username,
      email: user.email,
    };
  }
}
